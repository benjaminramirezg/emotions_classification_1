from sys import argv

P = []
y = []

y_file = argv[1]
p_file = argv[2]

with open(p_file) as p:
    for line in p:
        line = line.strip('\n')
        P.append(line)

with open (y_file) as ys:
    for l in ys:
        l = l.strip('\n')
        y.append(l)


n=len(y)-1
acierto = 0
for i in (range(0,n)):
    if P[i] == y[i]:
        acierto = acierto + 1
acierto_porcentual = 100*acierto/(n+1)
print("Total right answers: "+str(acierto))
print("Right answers percentage: " + "%.2f" % acierto_porcentual)

