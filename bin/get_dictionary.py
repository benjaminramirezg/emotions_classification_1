import requests, json, re
from sys import argv

#############
### VARIABLES
#############

texts_file=argv[1]
improved=argv[2]

Dic={}

# For API

def ask_api(text):
    oauth_token = ''
    endpoint = "https://svc02.api.bitext.com/parsing/"
    headers = { "Authorization" : "bearer " + oauth_token, "Content-Type" : "application/json" }
    params = {"language" : "eng","text" : text, "mode": "reviews"}
    res = requests.post(endpoint, headers=headers, data=json.dumps(params))
    resultid = json.loads(res.text).get('resultid')
    analysis = None
    while analysis == None:
        res = requests.get(endpoint + resultid + '/', headers=headers)
        if res.status_code == 200 :
            analysis = res.text
            json_analysis= json.loads(analysis).get('pos_lemma')
            return json_analysis

def get_pos_lemmas(text):
    analysis=ask_api(text)
    pos_lemmas=[]
    for sentence in analysis:
        for word in sentence:
            lemma=word.get('lemma')
            pos=word.get('pos')
            match=re.match('^(noun|adjective|verb|adverb)$',pos)
            if match:
                pos_lemma=lemma + "#" + pos
                pos_lemmas.append(pos_lemma)
    return pos_lemmas

def normalize_word(word):
    word=word.lower()
    word = re.sub("^\s*[,.]+", "", word)
    word = re.sub("[,.]+\s*$", "", word)
    return word

def get_normalized_words(text):
    words=text.split(' ')
    normalized_words=[]
    for word in words:
        normalized_word=normalize_word(word)
        normalized_words.append(normalized_word)
    return normalized_words

with open(texts_file) as f:
    for line in f:
        line = line.strip('\n')
        fields=line.split('\t')
        text=fields[10]
        emotion=fields[5]
        features=[]
        if improved == 1:
            features=get_pos_lemmas(text)
        else:
            features=get_normalized_words(text)

        for feature in features:
            if feature not in Dic:
                Dic[feature]=0
            Dic[feature] = Dic[feature] + 1

c=0
for w in sorted(Dic, key=Dic.get, reverse=True):
    c= c + 1
    if c == 201:
        break
    print(w)
