#!/usr/bin/python
from fann2 import libfann
from sys import argv

X_file=argv[1]
NN_file=argv[2]

def to_one_zero(l):
    l_clean=[]
    for n in l:
        if n == max(l):
            n = 1
        else:
            n = 0
        l_clean.append(n)
    return l_clean

ann = libfann.neural_net()
ann.create_from_file(NN_file)

with open (X_file) as Xc:
    for line in Xc:
        line = line.rstrip('\n')
        line = line.split(' ')
        line = [int(x) for x in line]
        a = ann.run(line)
        a=to_one_zero(a)
        a = ' '.join(str(x) for x in a)
        print(a)
