import requests, json, re, sys
from sys import argv

#############
### VARIABLES
#############

Words=[]
Emotions=['Aggression', 'Ambiguous', 'Anger', 'Anticipation', 'Awe', 'Contempt', 'Disapproval', 'Disgust', 'Fear', 'Joy', 'Love', 'Neutral', 'Optimism', 'Remorse', 'Sadness', 'Submission', 'Surprise', 'Trust']

texts_file=argv[1]
dictionary_file=argv[2]
improved=argv[3]

#############
### FUNCTIONS
#############

def ask_api(text):
    oauth_token = ''
    endpoint = "https://svc02.api.bitext.com/parsing/"
    headers = { "Authorization" : "bearer " + oauth_token, "Content-Type" : "application/json" }
    params = {"language" : "eng","text" : text, "mode": "reviews"}
    res = requests.post(endpoint, headers=headers, data=json.dumps(params))
    resultid = json.loads(res.text).get('resultid')
    analysis = None
    while analysis == None:
        res = requests.get(endpoint + resultid + '/', headers=headers)
        if res.status_code == 200 :
            analysis = res.text
            json_analysis= json.loads(analysis).get('pos_lemma')
            return json_analysis

def get_pos_lemmas(text):
    pos_lemmas=[]
    analysis=ask_api(text)
    for sentence in analysis:
        for word in sentence:
            lemma=word.get('lemma')
            pos=word.get('pos')
            match=re.match('^(noun|adjective|verb|adverb)$',pos)
            if match:
                pos_lemma=lemma + "#" + pos
                pos_lemmas.append(pos_lemma)
    return pos_lemmas

def normalize_word(word):
    word=word.lower()
    word = re.sub("^\s*[,.]+", "", word)
    word = re.sub("[,.]+\s*$", "", word)
    return word

def get_normalized_words(text):
    words=text.split(' ')
    normalized_words=[]
    for word in words:
        normalized_word=normalize_word(word)
        normalized_words.append(normalized_word)
    return normalized_words

#########
### MAIN
#########

with open(dictionary_file) as f:
    for line in f:
        line = line.strip('\n')
        Words.append(line)


with open(texts_file) as f:
    for line in f:
        line = line.strip('\n')
        fields=line.split('\t')
        text=fields[10]
        emotion=fields[5]
        features=[]

        if improved==1:
            features=get_pos_lemmas(text)
        else:
            features=get_normalized_words(text)

        X=[]
        for word in Words:
            if word in features:
                X.append(1)
            else:
                X.append(0)
        y=[]
        for e in Emotions:
            if e == emotion:
                y.append(1)
            else:
                y.append(0)
        Xstring=' '.join([str(x) for x in X])
        ystring=' '.join([str(i) for i in y])
        print(Xstring)
        print(ystring)
