#!/usr/bin/python
from sys import argv
from fann2 import libfann

X_file=argv[1]
NN_file=argv[2]

connection_rate = 1
learning_rate = 0.7
num_input = 200
num_hidden1 = 100
num_output = 18

desired_error = 0.0001
#max_iterations = 100000
max_iterations = 2000
iterations_between_reports = 1000

ann = libfann.neural_net()
ann.create_sparse_array(connection_rate, (num_input, num_hidden1, num_output))
ann.set_learning_rate(learning_rate)
ann.set_activation_function_output(libfann.SIGMOID)

ann.train_on_file(X_file, max_iterations, iterations_between_reports, desired_error)

ann.save(NN_file)
